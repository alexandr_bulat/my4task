package com.example.abulat.four;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.abulat.four.arrray.Realmy;
import com.example.abulat.four.db.Note;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.ArrayList;
import java.util.List;

import com.example.abulat.four.adapters.TestAdapter;
import com.example.abulat.four.arrray.Contacts;
import com.example.abulat.four.arrray.Contacts1;

import io.realm.Realm;


public class MainActivity extends AppCompatActivity {
    ListView listView;

    RuntimeExceptionDao<Contacts, Integer> cont;
    Contacts contacts;
    Dao<Contacts, Integer> userDao = null;
    DatabaseHelper helper;
    Bitmap b;
    private byte[] img = null;
    int curren_img = 0;
    private Realm mRealm;
    ArrayList<Mydata> mydatas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        mRealm = Realm.getInstance(this);

    }

    public void UsersofORM() {


        helper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        cont = helper.getContcRuntimeExcepctionDao();
        cont.create(new Contacts("Arcadii", R.drawable.arubailo, 47.009929, 28.858477));
        cont.create(new Contacts("Dima", R.drawable.me, 47.013060, 28.871459));
        cont.create(new Contacts("Alex", R.drawable.b, 47.009373, 28.854743));
        cont.create(new Contacts("Fil", R.drawable.fil, 47.013513, 28.855644));

        List<Contacts> notes = cont.queryForAll();

        listView.setAdapter(new TestAdapter(this, notes));
        OpenHelperManager.releaseHelper();


    }


    @Override

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_item, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.item1:
                UsersofORM();
                Toast.makeText(this, "ORMLITE", Toast.LENGTH_SHORT).show();

                break;

            case R.id.item2:
                UserofGREENDAO();
                Toast.makeText(this, "GREENDAO", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item3:
                // UserofREALM();
                Toast.makeText(this, "REALM", Toast.LENGTH_SHORT).show();
                break;

            case R.id.item4:
                UsersofMEMORY();
                Toast.makeText(this, "MEMORY", Toast.LENGTH_SHORT).show();
                break;

        }

        return super.onOptionsItemSelected(item);

    }

    public void UserofGREENDAO() {

        MyApllication.getNoteDao().insertInTx(new Note("Alex", R.drawable.arubailo, 47.009929, 28.858477, 0));
        MyApllication.getNoteDao().insertInTx(new Note("Dima", R.drawable.arubailo, 47.013060, 28.871459, 0));
        MyApllication.getNoteDao().insertInTx(new Note("ARCADII", R.drawable.arubailo, 47.009373, 28.854743, 0));
        MyApllication.getNoteDao().insertInTx(new Note("FIL", R.drawable.arubailo, 47.013513, 28.855644, 0));
        List<Note> list = MyApllication.getNoteDao().loadAll();

        listView.setAdapter(new TestAdapter(this, list));


    }

    public void UserofREALM() {
       /* List<Realmy> realmies = new ArrayList<Realmy>();

        mRealm.beginTransaction();
        Realmy book = mRealm.createObject(Realmy.class);
        book.setName("Alex");
        book.setImage(R.drawable.b);
        book.setName("Alex");
        book.setImage(R.drawable.b);
        book.setName("Alex");
        book.setImage(R.drawable.b);
        book.setName("Alex");
        book.setImage(R.drawable.b);

        realmies.add(new Realmy("Alex", R.drawable.b));
        realmies.add(new Realmy("Arcadii", R.drawable.arubailo));
        realmies.add(new Realmy("DIma", R.drawable.me));
        realmies.add(new Realmy("Fil", R.drawable.fil));
        mRealm.commitTransaction();


        listView.setAdapter(new  TestAdapter(this, realmies));*/

    }

    public void UsersofMEMORY() {
        List<Contacts1> notes = new ArrayList<Contacts1>();

        notes.add(new Contacts1("Arcadii", R.drawable.arubailo, 47.009929, 28.858477));
        notes.add(new Contacts1("Dima", R.drawable.me, 47.013060, 28.871459));
        notes.add(new Contacts1("Alex", R.drawable.b, 47.009373, 28.854743));
        notes.add(new Contacts1("Fil", R.drawable.fil, 47.013513, 28.855644));
        listView.setAdapter(new TestAdapter(this, notes));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}
