package com.example.abulat.four;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import com.example.abulat.four.arrray.Contacts;

/**
 * Created by abulat on 7/13/16.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ormlite.db";
    private static final int DATABASE_VERSION = 3;


    private Dao<Contacts, Integer> mUserDao = null;
    private RuntimeExceptionDao<Contacts, Integer> cont;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.armite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Contacts.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Contacts.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /* User */

    public Dao<Contacts, Integer> getUserDao() throws SQLException {
        if (mUserDao == null) {
            mUserDao = getDao(Contacts.class);
        }

        return mUserDao;
    }

    public RuntimeExceptionDao<Contacts, Integer> getContcRuntimeExcepctionDao() {
        if (cont == null) {
            cont = getRuntimeExceptionDao(Contacts.class);
        }
        return cont;
    }

    @Override
    public void close() {
        mUserDao = null;

        super.close();
    }
}