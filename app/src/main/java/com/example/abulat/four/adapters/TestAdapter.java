package com.example.abulat.four.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abulat.four.Maps;
import com.example.abulat.four.Mydata;
import com.example.abulat.four.R;

import java.util.List;

/**
 * Created by abulat on 7/13/16.
 */
public class TestAdapter extends BaseAdapter {

    List<? extends Mydata> listItem;

    Context mContext;

    public TestAdapter(Context mContext, List<? extends Mydata> listItem) {

        this.mContext = mContext;
        this.listItem = listItem;

    }


    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int position) {
        return listItem.get(position);
    }


    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View arg1, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_gate, viewGroup, false);


        TextView nameuser = (TextView) row.findViewById(R.id.nameuser);
        final Button buttonmap = (Button) row.findViewById(R.id.buttonmap);
        final ImageView imageuser = (ImageView) row.findViewById(R.id.imageuser);


        final Mydata contacts1 = listItem.get(position);
        buttonmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Maps.class);
                intent.putExtra("width", contacts1.getLagheigh());
                intent.putExtra("heigh", contacts1.getLagwidth());
                intent.putExtra("image", contacts1.getImage());
                intent.putExtra("name", contacts1.getName());
                mContext.startActivity(intent);


            }
        });


        imageuser.setImageResource(contacts1.getImage());

        nameuser.setText(contacts1.getName());

        return row;
    }
}