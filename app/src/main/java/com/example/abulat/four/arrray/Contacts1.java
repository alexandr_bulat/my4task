package com.example.abulat.four.arrray;

import com.example.abulat.four.Mydata;

/**
 * Created by abulat on 7/14/16.
 */
public class Contacts1 implements Mydata {
    public static final String TABLE_NAME_USERS = "users";

    public static final String FIELD_NAME_ID = "id";

    private int mId;

    private String name;

    private int image;
    private double lagwidth;
    private double lagheight;


    public Contacts1() {

    }

    public Contacts1(String name, int image, double lagheight, double lagwidth) {

        this.name = name;
        this.image = image;
        this.lagheight = lagheight;
        this.lagwidth = lagwidth;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getImage() {
        return image;
    }

    @Override
    public double getLagwidth() {
        return lagwidth;
    }

    @Override
    public double getLagheigh() {
        return lagheight;
    }


    public void setLagheight(int lagheight) {
        this.lagheight = lagheight;
    }

    public void setLagwidth(int lagwidth) {

        this.lagwidth = lagwidth;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "mId=" + mId +
                ", name='" + name + '\'' +
                ", image=" + image +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setImage(int image) {
        this.image = image;
    }

}


