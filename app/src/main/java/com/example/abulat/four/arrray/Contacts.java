package com.example.abulat.four.arrray;

import com.example.abulat.four.Mydata;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by abulat on 7/13/16.
 */

public class Contacts implements Mydata {
    public static final String TABLE_NAME_USERS = "users";

    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_NAME = "name";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private int mId;
    @DatabaseField
    private String name;
    @DatabaseField
    private int image;
    @DatabaseField
    private double lagwidth;
    @DatabaseField
    private double lagheight;

    public Contacts() {

    }

    public Contacts(String name, int image, double lagheight, double lagwidth) {

        this.name = name;
        this.image = image;
        this.lagheight = lagheight;
        this.lagwidth = lagwidth;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getImage() {
        return image;
    }

    public void setLagwidth(double lagwidth) {
        this.lagwidth = lagwidth;
    }

    public void setLagheight(double lagheight) {
        this.lagheight = lagheight;
    }

    public double getLagheight() {

        return lagheight;
    }


    @Override
    public double getLagwidth() {
        return lagwidth;
    }

    @Override
    public double getLagheigh() {
        return lagheight;
    }


    @Override
    public String toString() {
        return "Contacts{" +
                "mId=" + mId +
                ", name='" + name + '\'' +
                ", image=" + image +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
