package com.example.abulat.four.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abulat.four.R;
import com.example.abulat.four.db.Note;

import java.util.List;

/**
 * Created by abulat on 7/13/16.
 */
public class TestAdapterDEO extends BaseAdapter {
    List<Note> listItem;
    Context mContext;

    public TestAdapterDEO(Context mContext, List<Note> listItem) {
        this.mContext = mContext;
        this.listItem = listItem;

    }

    public int getCount() {
        return listItem.size();
    }

    public Object getItem(int arg0) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View arg1, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_gate, viewGroup, false);


        TextView nameuser = (TextView) row.findViewById(R.id.nameuser);
        final Button buttonmap = (Button) row.findViewById(R.id.buttonmap);
        final ImageView imageuser = (ImageView) row.findViewById(R.id.imageuser);


        final Note contacts1 = listItem.get(position);


        nameuser.setText(contacts1.getName());
        imageuser.setImageResource(contacts1.getImage());


        return row;
    }
}

