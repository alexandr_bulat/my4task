package com.example.abulat.four;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.example.abulat.four.db.DaoMaster;
import com.example.abulat.four.db.DaoSession;
import com.example.abulat.four.db.NoteDao;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by abulat on 7/13/16.
 */
public class MyApllication extends Application {


    static NoteDao noteDao;

    public static NoteDao getNoteDao() {
        return noteDao;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "blog-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        noteDao = daoSession.getNoteDao();
        /*RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);*/

    }
}
